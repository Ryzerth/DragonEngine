#pragma once
#include <string>
#include "mesh.h"

using namespace std;

class MeshManagerClass {
public:
	void AddMesh(Mesh mesh, string id);
	Mesh* Get(string id);
};

extern MeshManagerClass MeshManager;