#pragma once
#include <string>
#include <SDL2/SDL.h>

using namespace std;

class Display {
public:
	Display(int width, int height, string title);
	void Update();
	void Clear(float r, float g, float b, float a);
	void CenterMouse();
	void SetFullscreen(bool state);
	virtual ~Display();
	bool isClosed = false;
	int Width;
	int Height;
	int Ticks;
	float Delta;
	float FPS;

private:
	SDL_Window* _window;
	SDL_GLContext _glContext;
	int lastTicks;
};