#pragma once
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <cmath>

using namespace std;

class Camera
{
public:
	inline Camera(const glm::vec3& position, const glm::vec3& rotation, float fov, float aspect, float zNear, float zFar) {
		_perspective = glm::perspective(glm::radians(fov), aspect, zNear, zFar);
		Position = position;
		Rotation = rotation;
		_aspect = aspect;
		_zNear = zNear;
		_zFar = zFar;
		_forward = glm::vec3(0.0f, 0.0f, 1.0f); // 1 last
		_up = glm::vec3(0.0f, 1.0f, 0.0f); // 1 middle
	}

	inline glm::mat4 GetViewProjection() {
		Rotation.x = glm::clamp(Rotation.x, -90.0f, 90.0f);

		glm::vec3 front;
		front.x = cos(glm::radians(Rotation.x)) * cos(glm::radians(Rotation.y));
		front.y = sin(glm::radians(Rotation.x));
		front.z = cos(glm::radians(Rotation.x)) * sin(glm::radians(Rotation.y));
		_forward = glm::normalize(front);

		glm::vec3 up;
		up.x = cos(glm::radians(Rotation.x + 90.0f)) * cos(glm::radians(Rotation.y));
		up.y = sin(glm::radians(Rotation.x + 90.0f));
		up.z = cos(glm::radians(Rotation.x + 90.0f)) * sin(glm::radians(Rotation.y));
		_up = glm::normalize(up);

		glm::mat4 lookAt = glm::lookAt(Position, Position + _forward, _up);
		return _perspective * lookAt;
	}

	inline void SetFov(float fov) {
		_fov = fov;
		_perspective = glm::perspective((float)((fov * 3.14159265) / 180), _aspect, _zNear, _zFar);
	}

	inline void SetAspectRatio(float aspect) {
		_aspect = aspect;
		_perspective = glm::perspective((float)((_fov * 3.14159265) / 180), aspect,_zNear, _zFar);
	}

	glm::vec3 Position;
	glm::vec3 Rotation;

	inline ~Camera() {

	}

private:
	glm::mat4 _perspective;
	glm::vec3 _forward;
	glm::vec3 _up;
	float _aspect;
	float _zNear;
	float _zFar;
	float _fov;
};

