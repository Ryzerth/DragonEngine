#pragma once
#include <string>

using namespace std;

class LoggerClass {
public:
	LoggerClass();
	void Log(string str);
	void Ok();
	void Warn();
	void Failed();
	void Panic(string str);
};

extern LoggerClass Logger;