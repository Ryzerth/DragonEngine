#pragma once
#include <string>
#include "texture.h"

using namespace std;

class TextureManagerClass {
public:
	void AddTexture(Texture texture, string id);
	Texture* Get(string id);
};

extern TextureManagerClass TextureManager;