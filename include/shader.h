#pragma once
#include <string>
#include <GL/glew.h>
#include "transform.h"
#include "camera.h"

using namespace std;

class Shader {
public:
	Shader(string vertPath, string fragPath, string name);
	inline Shader() {};
	void Bind();
	void UpdateUniforms(Transform& transform, Camera& camera);
	string Name;
	~Shader();

	

private:
	enum {
		VERTEX_SHADER,
		FRAGMENT_SHADER,
		SHADER_COUNT
	};
	enum { // Uniforms
		TRANSFORM_U,
		VIEWMATRIX_U,
		CAMERAPOS_U,
		UNIFORM_COUNT
	};
	GLuint _program;
	GLuint _shaders[SHADER_COUNT];
	GLuint _uniforms[UNIFORM_COUNT];
};

