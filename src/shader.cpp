#include "shader.h"
#include <fstream>
#include <streambuf>
#include "logger.h"
#include "camera.h"

static string readFile(string path);
static string checkShader(GLuint shader, GLuint flag, bool isProgram);
static GLuint createShader(string code, GLenum type);

Shader::Shader(string vertPath, string fragPath, string name) {
	this->Name = name;

	_program = glCreateProgram();
	Logger.Log("(" + name + ") Loading vertex shader...");
	string vertCode = readFile(vertPath);
	if (vertCode == "") {
		Logger.Failed();
		Logger.Panic("(" + name + ") Could not load vertex shader: File not found!");
	}
	Logger.Ok();

	Logger.Log("(" + name + ") Loading fragment shader...");
	string fragCode = readFile(fragPath);
	if (fragCode == "") {
		Logger.Failed();
		Logger.Panic("(" + name + ") Could not load vertex shader: File not found!");
	}
	Logger.Ok();

	Logger.Log("(" + name + ") Compiling vertex shader...");
	_shaders[VERTEX_SHADER] = createShader(vertCode, GL_VERTEX_SHADER);
	if (_shaders[VERTEX_SHADER] == 0) {
		Logger.Failed();
		Logger.Panic("(" + name + ") Could not create vertex shader!");
	}
	string vertexErrors = checkShader(_program, GL_COMPILE_STATUS, false);
	if (vertexErrors != "") {
		Logger.Failed();
		Logger.Panic("(" + name + ") Could not compile vertex shader: " + vertexErrors);
	}
	Logger.Ok();

	Logger.Log("(" + name + ") Compiling fragment shader...");
	_shaders[FRAGMENT_SHADER] = createShader(fragCode, GL_FRAGMENT_SHADER);
	if (_shaders[VERTEX_SHADER] == 0) {
		Logger.Failed();
		Logger.Panic("(" + name + ") Could not create fragment shader!");
	}
	string fragmentErrors = checkShader(_program, GL_COMPILE_STATUS, false);
	if (fragmentErrors != "") {
		Logger.Failed();
		Logger.Panic("(" + name + ") Could not compile fragment shader: " + fragmentErrors);
	}
	Logger.Ok();

	for (int i = 0; i < SHADER_COUNT; i++) {
		glAttachShader(_program, _shaders[i]);
	}

	glBindAttribLocation(_program, 0, "position"); // TODO: Make an enum
	glBindAttribLocation(_program, 1, "textureCoords");
	glBindAttribLocation(_program, 2, "normals");

	Logger.Log("(" + name + ") Linking shaders...");
	glLinkProgram(_program);
	string linkErrors = checkShader(_program, GL_LINK_STATUS, true);
	if (linkErrors != "") {
		Logger.Failed();
		Logger.Panic("(" + name + ") Could not link shaders: " + linkErrors);
	}
	Logger.Ok();

	Logger.Log("(" + name + ") Validating shaders...");
	glValidateProgram(_program);
	string validayeErrors = checkShader(_program, GL_VALIDATE_STATUS, true);
	if (validayeErrors != "") {
		Logger.Failed();
		Logger.Panic("(" + name + ") Could not validate shaders: " + validayeErrors);
	}
	Logger.Ok();

	_uniforms[TRANSFORM_U] = glGetUniformLocation(_program, "transform");
	_uniforms[VIEWMATRIX_U] = glGetUniformLocation(_program, "viewMatrix");
	_uniforms[CAMERAPOS_U] = glGetUniformLocation(_program, "cameraPos");
}

void Shader::Bind() {
	glUseProgram(_program);
}

void Shader::UpdateUniforms(Transform& transform, Camera& camera) {
	glm::mat4 model = transform.GetModel();
	glm::mat4 viewMatrix = camera.GetViewProjection();
	glUniformMatrix4fv(_uniforms[TRANSFORM_U], 1, GL_FALSE, &model[0][0]);
	glUniformMatrix4fv(_uniforms[VIEWMATRIX_U], 1, GL_FALSE, &viewMatrix[0][0]);
	glUniform3f(_uniforms[CAMERAPOS_U], camera.Position.x, camera.Position.y, camera.Position.z);
}

static GLuint createShader(string code, GLenum type) {
	GLuint shader = glCreateShader(type);
	if (shader == 0) {
		return 0;
	}
	const GLchar* _code[1];
	GLint codeLength[1];
	_code[0] = code.c_str();
	codeLength[0] = code.length();
	glShaderSource(shader, 1, _code, codeLength);
	glCompileShader(shader);
	return shader;
}

static string readFile(string path) {
	ifstream file;
	string out;
	string line;
	file.open(path);
	if (!file.is_open()) {
		return "";
	}
	while (file.good()) {
		getline(file, line);
		out += line + "\n";
	}
	return out;
}

static string checkShader(GLuint shader, GLuint flag, bool isProgram) {
	GLint success = 0;
	GLchar msg[2048] = { 0x00 };

	if (isProgram) {
		glGetProgramiv(shader, flag, &success);
	}
	else {
		glGetShaderiv(shader, flag, &success);
	}
	if (success) {
		return "";
	}
	if (isProgram) {
		glGetProgramInfoLog(shader, sizeof(msg), NULL, msg);
	}
	else {
		glGetShaderiv(shader, flag, &success);
	}
	return msg;
}

Shader::~Shader() {
	/*for (int i = 0; i < SHADER_COUNT; i++) {
		glDetachShader(_program, _shaders[i]);
		glDeleteShader(_shaders[i]);
	}
	glDeleteProgram(_program);*/
}