#include "texture.h"
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#include <GL/glew.h>
#include <assert.h>

Texture::Texture(string path) {
	int width, height, compCount;
	unsigned char* imageData = (unsigned char*)stbi_load(path.c_str(), &width, &height, &compCount, 4);
	if (imageData == NULL) {
		this->Width = -1;
		this->Height = -1;
		return;
	}
	this->Width = width;
	this->Height = height;
	glGenTextures(1, &_texture);
	glBindTexture(GL_TEXTURE_2D, _texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, imageData); // TODO: Change mipmap level (first 0)
	stbi_image_free(imageData);
}

void Texture::Bind(unsigned int unit) {
	assert(unit >= 0 && unit <= 31);
	glActiveTexture(GL_TEXTURE0 + unit);
	glBindTexture(GL_TEXTURE_2D, _texture);
}

Texture::~Texture() {
	//glDeleteTextures(1, &_texture);
}
