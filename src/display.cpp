#include "display.h"
#include <SDL2/SDL.h>
#include <GL/glew.h>
#include "logger.h"


Display::Display(int width, int height, string title) {
	Width = width;
	Height = height;
	Logger.Log("Creating window...");
	SDL_Init(SDL_INIT_EVERYTHING);
	Ticks = SDL_GetTicks();
	lastTicks = Ticks;
	SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_BUFFER_SIZE, 32);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 8);
	_window = SDL_CreateWindow(title.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, SDL_WINDOW_OPENGL);
	_glContext = SDL_GL_CreateContext(_window);
	GLenum status = glewInit();
	if (status != GLEW_OK) {
		Logger.Failed();
		Logger.Panic("Could not initialize OpenGL!");
	}
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	isClosed = false;
	SDL_WarpMouseInWindow(_window, width / 2, height / 2);
	SDL_SetRelativeMouseMode(SDL_TRUE);
	Logger.Ok();
}

long counter = 0;
float fpsBuf = 0.0f;

void Display::Update() {
	Ticks = SDL_GetTicks();
	Delta = Ticks - lastTicks;
	lastTicks = Ticks;
	fpsBuf += 1000.0f / Delta;
	SDL_GL_SwapWindow(_window);
	
	if (counter == 10) {
		counter = 0;
		FPS = fpsBuf / 10;
		fpsBuf = 0.0f;
		printf("%f\n", FPS);
	}
	counter++;
}

void Display::SetFullscreen(bool state) {
	if (state) {
		SDL_SetWindowFullscreen(_window, SDL_WINDOW_FULLSCREEN);
		SDL_GetWindowSize(_window, &Width, &Height);

	}
	else {
		SDL_SetWindowFullscreen(_window, 0);
		SDL_GetWindowSize(_window, &Width, &Height);
	}
}

void Display::CenterMouse() {
	SDL_WarpMouseInWindow(_window, 0, 0);
}

void Display::Clear(float r, float g, float b, float a) {
	glClearColor(r, g, b, a);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

Display::~Display() {
	SDL_GL_DeleteContext(_glContext);
	SDL_DestroyWindow(_window);
	SDL_Quit();
	isClosed = true;
}
