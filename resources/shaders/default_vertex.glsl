#version 120 // TODO: 330

varying vec2 _textureCoords;
varying vec3 _normals;
varying vec3 _toLight;
varying vec3 _toCamera;

attribute vec3 position;
attribute vec2 textureCoords;
attribute vec3 normals;

uniform mat4 transform;
uniform mat4 viewMatrix;
uniform vec3 cameraPos;

void main() {
	vec4 worldPos = transform * vec4(position, 1.0);
	gl_Position = viewMatrix * worldPos;
	_textureCoords = textureCoords;
	_normals = (transform * vec4(normals, 0.0)).xyz;
	_toLight = normalize(vec3(20.0, 10.0, 20.0) - worldPos.xyz);
	_toCamera = normalize(cameraPos - worldPos.xyz);
}