#version 120

varying vec2 _textureCoords;
varying vec3 _normals;
varying vec3 _toLight;
varying vec3 _toCamera;

uniform sampler2D diffuse;

void main() {
	float reflectivity = 1.0f;

	vec3 reflected = reflect(-_toLight, _normals);
	float specular = pow(max(dot(reflected, _toCamera), 0.0), 8.0);
	float brightness = clamp(dot(_toLight, _normals), 0.1, 1.0);
	vec3 light = brightness * vec3(1.0, 1.0, 1.0); // Color
	vec3 lightSpecular = reflectivity * specular * vec3(1.0, 1.0, 1.0); // Color
	gl_FragColor = vec4(1.0, 0.0, 0.0, 1.0) * (vec4(light, 1.0) + vec4(lightSpecular, 1.0));
}